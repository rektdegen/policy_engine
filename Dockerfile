# use python version 3 docker image as base image
FROM python:3

# cd to directory /usr/src/policy_engine. this will be our working directory
WORKDIR /usr/src/policy_engine

# copy the content of current directory in to the pwd of docker image.
# pwd in image is /usr/src/app
COPY . .

# now run the command to install dependencies from requirements file
RUN pip3 install --no-cache-dir -r requirements.txt
RUN python3 manage.py makemigrations
RUN python3 manage.py migrate
RUN python3 manage.py price_updater

#install cron 
RUN apt-get update && apt-get install -qq -y cron

# Add crontab file in the cron directory
ADD crontab /etc/cron.d/policy_engine

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/policy_engine

# Apply cron job
RUN crontab /etc/cron.d/policy_engine

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Run the command on container startup
CMD ["cron", "-f"] && tail -f /var/log/cron.log


# expose the port 8800
EXPOSE 8080

# everytime container is started, run this command
CMD python3 manage.py runserver 0.0.0.0:8080 
