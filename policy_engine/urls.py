"""urls for django app"""
from django.contrib import admin
from django.conf.urls import url
import rest_api.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',
        rest_api.views.hello_view),  # hello_world
    url(r'^api/hello/(?P<world>[a-z]+)$',
        rest_api.views.hello_view),  # hello_world

    # these are functional calls
    url(r'^api/users$',
        rest_api.views.user_view),  # POST user
    url(r'^api/users/(?P<username>[a-z]+)$',
        rest_api.views.delete_user_view),  # DELETE user
    url(r'^api/addresses/(?P<username>[a-z]+)$',
        rest_api.views.address_create),  # POST address
    url(r'^api/addresses/(?P<username>[a-z]+)/(?P<address>[a-zA-Z0-9]+)$',
        rest_api.views.address_delete),  # delete address
    url(r'^api/quantities/(?P<username>[a-z]+)$',  # PUT; if quantity is -1 then is any quantity
        rest_api.views.quantity_put),  # PUT user quantity and fiat
    url(r'^api/transactions/(?P<username>[a-z]+)$',
        rest_api.views.transaction_create),
    # these display paginated data
    url(r'^api/transactions/(?P<status>[a-z]+)/(?P<page>[0-9]+)$',
        rest_api.views.get_transaction_view),  # GET with filter; outgoing, rejected, all
    url(r'^api/users/(?P<page>[0-9]+)$',
        rest_api.views.get_users_view),  # GET with page
]
