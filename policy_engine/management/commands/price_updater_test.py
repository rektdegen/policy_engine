"""test case"""
import json
import requests

def get_fiat_rates():
    """prints results when run"""
    complete_url = 'https://blockchain.info/ticker'
    data_return = requests.Session().get(complete_url)
    data = json.loads(data_return.content.decode("utf-8"))
    print(data['USD']['last'])


if __name__ == "__main__":
    get_fiat_rates()
