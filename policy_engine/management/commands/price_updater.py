"""run this con cron"""
import json
import decimal
import requests
from django.core.management.base import BaseCommand
from rest_api.models import Price


def get_fiat_rates():
    """pulls btc price and update price object"""
    complete_url = 'https://blockchain.info/ticker'
    data_return = requests.Session().get(complete_url)
    data = json.loads(data_return.content.decode("utf-8"))
    price = decimal.Decimal(data['USD']['last'])

    exchange_rate_data = Price.objects.filter(crypto_ticker='btc',
                                              fiat_ticker='usd')

    if exchange_rate_data.exists():
        for new_data in exchange_rate_data:
            new_data.crypto_price = price
    else:
        new_exchange_rate = Price(crypto_ticker='btc',
                                  fiat_ticker='usd',
                                  crypto_price=price)
        new_exchange_rate.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        get_fiat_rates()
