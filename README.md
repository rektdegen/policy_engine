# Basic Policy Server

The policy engine is a backend service that manages bitcoin transactions’ lifecycle. When a user wants to issue an outgoing bitcoin transaction they make a transaction creation request to the policy service. The policy service evaluates that transaction against a predefined set of transaction policy rules. If the transaction matches a rule, it is approved and sent to the blockchain for processing. If it does not match any rule, it is rejected.


### Design Notes

I included all the specs:
    
* multiuser support
* multicurrency support
* cron process to pull the bitcoin price from the internet every 5 minutes
* CRUD
* pagination
* transaction filters (accept, reject any)
* wildcard address  (any)
* -1 withdrawal quantity (default) for withdrawing without limit
* admin GUI

This project was built in a few hours to satisfy a coding challange for a programming job. It is obviously not suitable for production. Please don't do that. 

The code is linted, as always I try to follow best practices such as:
    
* PEP 8 style https://www.python.org/dev/peps/pep-0008/

For simplicity I did not use any extra libs such as [Django Rest Framework](https://www.django-rest-framework.org/). That is something I might use in the future. 

Requests and cron to pull bitcoin data from https://blockchain.info/ticker every 5 minutes.

This code is not perfect. If I were to continue working on it there are some things to improve with, such as:

* Refactoring views.py 
    * Class based view to reduce code redundantcy and complexity

* Refactor url.py
    * Probably change some things with naming to comply with REST best practices

* API explorer - there are few options for this
    * Django REST Framework
    * Swagger

### Prerequisites

Docker - https://docs.docker.com/install/

### Deployment

Clone this:

    $ git clone https://rektdegen@bitbucket.org/rektdegen/policy_engine.git
 
Move to directory:

    $ cd policy_engine

Build a new image:

    $ sudo docker build policy_engine .

Running the container:
    
    $ sudo docker run -p 8080:8080 policy_engine
    
### Interaction

I included a Python API class for interaction [here](rest_api/tests.py).
It is currently incomplete but here are a some methods:

* Create User <user>
* Delete User <user>
* Create Address <user> <address>
* Delete Address <user> <address>
* Update Quantity <user> <quantity> <ticker>
    * This updates withdrawal limit and you can specify either 'usd' or 'btc'

* Send Transaction <user> <quantity> <address> 
    * This attempts to send a transaction if it passes the rules it will be marked as accepted otherwise marked as rejected

* Get Addresses <user> <page>
    * Supports up to 10 addresses per page TODO

* List Transactions <all|rejected|accepted> <page>
    * 10 transactions per page
    * Filter based results

* List Users <page>
    * 10 users per page
    * View list of users, max withdrawal and associated addresses

To run the tests:
    
    $ python3 tests.py
    
## Authors

* **Rekt Degen** - [Blog and Research](https://rektdegen.wordpress.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Credit

Thank you anuragrana for providing a great docker example. [link](https://github.com/anuragrana/docker-django-example)
Also thanks to Ekito for configuring cron in docker [link](https://github.com/Ekito/docker-cron)
