"""all the views, i would refactor as a class maybe to reduce complixiy and repeatitions"""
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import QueryDict
from rest_api.models import Transaction, User, Destination
from rest_api.validate import validate_transaction


def hello_view(request, world):
    """hello world test"""
    reply = [{'message': 'hello'}]
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def user_view(request, username=None):
    """create user"""
    reply = []
    error = {'error': None}
    message = {'message': None}

    if request.method == 'POST':  # create user
        username = request.POST.get('username', '')
        if not User.objects.filter(username=username).exists():
            new_user = User(username=username, quantity=-1, ticker='usd')
            new_user.save()
            message['message'] = 'user %s created' % username
        else:
            error['error'] = 'username already registered'

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def delete_user_view(request, username=None):
    """delete a user and his whitelist"""
    reply = []
    error = {'error': None}
    message = {'message': None}

    if request.method == 'DELETE':  # delete user
        delete_object_list = User.objects.filter(username=username)
        delete_destination_list = Destination.objects.filter(user=username)
        if delete_object_list.exists():
            [e.delete() for e in delete_object_list]
            [e.delete() for e in delete_destination_list]
            message['message'] = 'user %s deleted' % username
        else:
            error['error'] = 'username not found'

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def quantity_put(request, username):
    """update user max withdrawal and currency"""
    reply = []
    error = {'error': None}
    message = {'message': None}
    put = QueryDict(request.body)
    # this is a put request
    quantity = put.get('quantity')
    ticker = put.get('ticker')
    if User.objects.filter(username=username):
        user = User.objects.filter(username=username)[0]
        user.quantity = quantity
        user.ticker = ticker
        user.save()
        message['message'] = 'quantity set to %s, ticker set to %s' % (quantity, ticker)
    else:
        error['error'] = 'username does not exist'

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def address_create(request, username):
    """create address"""
    reply = []
    error = {'error': None}
    message = {'message': None}

    if request.method == 'POST':  # create address

        address = request.POST.get('address', '')
        destination_list = Destination.objects.filter(user=username,
                                                      address=address)
        if not destination_list.exists():
            new_destination = Destination(user=username, address=address)
            new_destination.save()
            message['message'] = 'destination %s created for %s' % (address, username)
        else:
            error['error'] = 'Destination already exists'

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def address_delete(request, username, address):
    """delete address"""
    reply = []
    error = {'error': None}
    message = {'message': None}

    if request.method == 'DELETE':  # delete address
        destination_list = Destination.objects.filter(user=username,
                                                      address=address)
        if destination_list.exists():
            destination_list[0].delete()
            message['message'] = 'destination %s deleted for %s' % (address, username)

        else:
            error['error'] = 'Destination does not exist'

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


@csrf_exempt
def transaction_create(request, username):
    """attempt to create a transaction
    pass to verify function
    mark as accept or reject"""
    reply = []
    error = {'error': None}
    message = {'message': None}

    if request.method == 'POST':  # create user
        quantity = request.POST.get('quantity', '')
        address = request.POST.get('address', '')
        status = validate_transaction(username, quantity, address)
        new_transaction = Transaction(user=username,
                                      quantity=quantity,
                                      address=address,
                                      status=status)
        new_transaction.save()
        message['message'] = '%s to %s is %s' % (quantity, address, status)
    else:
        error['error'] = 'invalid parameters'
    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


def get_transaction_view(request, status, page):
    """this method returns paginated transactions"""
    page = int(page)
    reply = []
    error = {'error': None}
    message = {'message': {'transactions': [], 'total': {}}}

    if status == 'all':
        transactions = Transaction.objects.all()
        transactions_page = transactions[10*(page-1):10*(page)]
    else:
        transactions = Transaction.objects.filter(status=status)
        transactions_page = transactions[10*(page-1):10*(page)]
    print(len(transactions))
    for transaction in transactions_page:
        transaction_info = {'transaction_id': transaction.id,
                            'quantity': '%8f' % transaction.quantity,
                            'address': transaction.address,
                            'status': transaction.status
                            }

        message['message']['transactions'].append(transaction_info)
    message['message']['total_transactions'] = transactions.count()

    reply.append(error)
    reply.append(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')


def get_users_view(request, page):
    """this methon returns paginated user policy with whitelist"""
    page = int(page)
    reply = []
    error = {'error': None}
    message = {'message': {'users': [], 'total': {}}}

    users = User.objects.all()
    users_page = users[10*(page-1):10*(page)]

    for user in users_page:
        user_info = {'user_id': user.id,
                     'quantity': '%8f' % user.quantity,
                     'ticker': user.ticker,
                     'whitelist': []
                     }

        destinations = Destination.objects.filter(user=user.username)
        for destination in destinations:
            user_info['whitelist'].append(destination.address)

        message['message']['total_users'] = users.count()
        message['message']['users'].append(user_info)

    reply.append(error)
    reply.append(message)
    print(message)
    return HttpResponse(json.dumps(reply), content_type='application/json')
