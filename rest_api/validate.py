"""valiation script for sending transaction"""
from decimal import Decimal
from rest_api.models import User, Destination, Price


def validate_transaction(username, quantity, address):
    """lookup user rules
    check transaction against rules
    mark user"""
    quantity = Decimal(quantity)
    valid = 'reject'
    user = User.objects.filter(username=username)[0]
    address_data = Destination.objects.filter(address=address,
                                              user=username)
    address_any = Destination.objects.filter(address='all',
                                             user=username)
    price = Price.objects.get(crypto_ticker='btc',
                              fiat_ticker='usd')

    if address_data.exists() or address_any.exists():
        if user.quantity == -1:
            valid = 'accept'
        elif user.ticker == 'btc' and quantity <= user.quantity:
            valid = 'accept'
        elif user.ticker == 'usd' and quantity <= user.quantity / price.crypto_price:
            valid = 'accept'

    return valid
