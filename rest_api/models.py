"""models for policy engine"""
from django.db import models


class Transaction(models.Model):
    """i would probably add a date field"""
    user = models.CharField(max_length=20)
    quantity = models.DecimalField(max_digits=28,
                                   decimal_places=8)
    address = models.CharField(max_length=35)  # bitcoin address len is 26-35 alp-num chars
    status = models.CharField(max_length=6)  # accept or reject


class User(models.Model):
    """user models quantity is defined up to 8 decimal digits"""
    username = models.CharField(max_length=20)  # name for aliasing
    quantity = models.DecimalField(max_digits=28,
                                   decimal_places=8)
    ticker = models.CharField(max_length=3)  # btc or usd

class Destination(models.Model):
    """maybe many to one relationshup with User class idk"""
    address = models.CharField(max_length=35)
    user = models.CharField(max_length=20)

class Price(models.Model):
    """pretty standard, you can updated with with a managment command
    add more ojects for different currencies"""
    crypto_ticker = models.CharField(max_length=3)
    fiat_ticker = models.CharField(max_length=3)
    crypto_price = models.DecimalField(max_digits=28,
                                       decimal_places=8)
