"""basic stuff for admin views"""
from django.contrib import admin
from rest_api.models import Transaction, User, Destination, Price

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'quantity', 'address', 'status')


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'quantity', 'ticker')


class DestinationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'address')


class PriceAdmin(admin.ModelAdmin):
    list_display = ('crypto_ticker', 'fiat_ticker', 'crypto_price')

admin.site.register(Transaction, TransactionAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Destination, DestinationAdmin)
admin.site.register(Price, PriceAdmin)
