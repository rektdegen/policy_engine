# server Tests
import json
import requests

# some constants to use in tests
USERNAME = 'testuser'
URL = "http://127.0.0.1:8080"
LIST_OF_WALLETS = ['385cR5DM96n1HvBDMzLHPYcw89fZAXULJP',
                   '3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r',
                   '3Nxwenay9Z8Lc9JBiywExpnEFiLp6Afp8v',
                   '3Cbq7aT1tY8kMxWLbitaG7yT6bPbKChq64',
                   '3AweAnU1qYSUCJ5Hvy9DFEB7dVqUebZw5i',
                   '34xp4vRoCGJym3xR7yCVPFHoCNxv4Twseo',
                   '183hmJGRuTEi2YDCWy5iozY8rZtFwVgahM',
                   '1FeexV6bAHb8ybZjqQMjJrcCrHGW9sb6uF',
                   '1HQ3Go3ggs8pFnXuHVHRytPCq5fGG8Hbhx',
                   '1LdRcdxfbSnmCYYNdeYpUnztiYzVfBEQeC',
                   '1JCe8z4jJVNXSjohjM4i9Hh813dLCNx2Sy']

LIST_OF_QUANTITIES = [(10000, 'usd'),
                      (4000, 'usd'),
                      (1.00004, 'btc'),
                      (1.00500002, 'btc')]


class Tests(object):

    def return_result(self, path, http_call, post_data):
        """push data to server returns results"""
        complete_url = URL + path
        #print(complete_url)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        if http_call == "GET":
            data_return = requests.get(complete_url)
        elif http_call == "POST":
            data_return = requests.post(complete_url, data=post_data, headers=headers)
        elif http_call == "PUT":
            data_return = requests.put(complete_url, data=post_data, headers=headers)
        elif http_call == "DELETE":
            data_return = requests.delete(complete_url)
        print(data_return.content)
        return json.loads(data_return.content.decode("utf-8"))

    def create_user(self, username):
        path = "/api/users"
        post_data = 'username=%s' % username
        return self.return_result(path, "POST", post_data)

    def delete_user(self, username):
        path = "/api/users/%s" % username
        post_data = ''
        return self.return_result(path, "DELETE", post_data)

    def create_address(self, username, address):
        path = "/api/addresses/%s" % username
        post_data = 'address=%s' % address
        return self.return_result(path, "POST", post_data)

    def delete_address(self, username, address):
        path = "/api/addresses/%s/%s" % (username, address)
        post_data = ''
        return self.return_result(path, "DELETE", post_data)

    def get_addresses(self, username, page):
        path = "/api/addresses/%s/%s" % (username, page)
        post_data = ''
        return self.return_result(path, "GET", post_data)

    def update_quantity(self, username, quantity, ticker):
        path = "/api/quantities/%s" % (username)
        post_data = 'quantity=%s&ticker=%s' % (quantity, ticker)
        return self.return_result(path, "PUT", post_data)

    def send_transaction(self, username, quantity, address):
        path = "/api/transactions/%s" % (username)
        post_data = 'quantity=%s&address=%s' % (str(quantity), address)
        return self.return_result(path, "POST", post_data)

    def list_transactions(self, status, page):
        path = "/api/transactions/%s/%s" % (status, page)
        post_data = ''
        return self.return_result(path, "GET", post_data)

    def list_users(self, page):
        path = "/api/users/%s" % (page)
        post_data = ''
        return self.return_result(path, "GET", post_data)


if __name__ == "__main__":

    Tests().create_user(USERNAME)

    #test policy management / transaction filtering
    Tests().create_address(USERNAME, LIST_OF_WALLETS[0])
    Tests().delete_address(USERNAME, LIST_OF_WALLETS[0])
    Tests().create_address(USERNAME, 'all')

    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])
    Tests().update_quantity(USERNAME, .5, 'btc')
    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])
    Tests().update_quantity(USERNAME, 500, 'usd')
    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])
    Tests().update_quantity(USERNAME, 50000, 'usd')
    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])
    Tests().delete_address(USERNAME, 'all')
    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])
    Tests().create_address(USERNAME, LIST_OF_WALLETS[0])
    Tests().send_transaction(USERNAME, 1.00004, LIST_OF_WALLETS[0])

    #test get status and pagination
    Tests().list_transactions('all', 1)
    Tests().list_transactions('all', 2)
    #test get user info
    for e in LIST_OF_WALLETS:
        Tests().create_address(USERNAME, e)
    Tests().list_users(1)
    Tests().list_users(2)

    Tests().delete_user(USERNAME)
